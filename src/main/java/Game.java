public class Game {
    private static Game game;
    private Robot robot1;
    private Robot robot2;
    private boolean state;

    private Game(){
        robot1 = new Robot();
        robot2 = new Robot();
        state = true;
    }

    public static Game getGame(){
        if(game == null){
            game = new Game();
        }
        return game;
    }


    public Robot getRobot(Integer number) {
        return (number == 1) ? robot1 : robot2;
    }


    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
