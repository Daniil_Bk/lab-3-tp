import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 8080;
    private static final int ID_PLAYER1 = 1;
    private static final int ID_PLAYER2 = 2;


    public static void main(String[] args) throws IOException {
        try {
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Server Started");
            Socket socket1 = server.accept();
            System.out.println("Client 1 connected to socket 1");
            Socket socket2 = server.accept();
            System.out.println("Client 2 connected to socket 2");
            BufferedReader inClient1 = new BufferedReader(new InputStreamReader(socket1.getInputStream()));
            BufferedWriter outClient1 = new BufferedWriter(new OutputStreamWriter(socket1.getOutputStream()));
            BufferedReader inClient2 = new BufferedReader(new InputStreamReader(socket2.getInputStream()));
            BufferedWriter outClient2 = new BufferedWriter(new OutputStreamWriter(socket2.getOutputStream()));

            Game game = Game.getGame();
            System.out.println("Game start");
            int round = 1;
            while (game.getState()) {
                System.out.println("Раунд " + round);
                JSONObject gameMessage = new JSONObject();
                gameMessage.put("id", 1);
                gameMessage.put("state", game.getState());
                gameMessage.put("hp", game.getRobot(1).getHp());
                gameMessage.put("energy", game.getRobot(1).getEnergy());
                gameMessage.put("hp_opponent", game.getRobot(2).getHp());
                gameMessage.put("energy_opponent", game.getRobot(2).getEnergy());
                gameMessage.put("last_action", game.getRobot(1).getState());
                gameMessage.put("last_action_opponent", game.getRobot(2).getState());

                outClient1.write(gameMessage.toString() + "\n");
                outClient1.flush();

                gameMessage.put("id", 2);
                gameMessage.put("hp", game.getRobot(2).getHp());
                gameMessage.put("energy", game.getRobot(2).getEnergy());
                gameMessage.put("hp_opponent", game.getRobot(1).getHp());
                gameMessage.put("energy_opponent", game.getRobot(1).getEnergy());
                gameMessage.put("last_action", game.getRobot(2).getState());
                gameMessage.put("last_action_opponent", game.getRobot(1).getState());


                outClient2.write(gameMessage.toString() + "\n");
                outClient2.flush();
                System.out.println("Ход игрока 1");
                JSONObject clientMessage1 = new JSONObject(inClient1.readLine());
//            System.out.println(inClient1.readLine());
                System.out.println("Ход игрока 2");
                JSONObject clientMessage2 = new JSONObject(inClient2.readLine());
                MessageAnalyzer.analyze(clientMessage1, clientMessage2);
                GameResponser.response();
//            System.out.println(inClient2.readLine());

                round++;

            }

            if(!game.getState()){
                JSONObject gameMessage = new JSONObject();
                gameMessage.put("id", 1);
                gameMessage.put("state", game.getState());
                gameMessage.put("hp", game.getRobot(1).getHp());
                gameMessage.put("energy", game.getRobot(1).getEnergy());
                gameMessage.put("hp_opponent", game.getRobot(2).getHp());
                gameMessage.put("energy_opponent", game.getRobot(2).getEnergy());
                gameMessage.put("last_action", game.getRobot(1).getState());
                gameMessage.put("last_action_opponent", game.getRobot(2).getState());

                outClient1.write(gameMessage.toString() + "\n");
                outClient1.flush();

                gameMessage.put("id", 2);
                gameMessage.put("hp", game.getRobot(2).getHp());
                gameMessage.put("energy", game.getRobot(2).getEnergy());
                gameMessage.put("hp_opponent", game.getRobot(1).getHp());
                gameMessage.put("energy_opponent", game.getRobot(1).getEnergy());
                gameMessage.put("last_action", game.getRobot(2).getState());
                gameMessage.put("last_action_opponent", game.getRobot(1).getState());


                outClient2.write(gameMessage.toString() + "\n");
                outClient2.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("Игра окончена");
    }
}

