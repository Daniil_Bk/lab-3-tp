import org.json.JSONObject;

public class MessageAnalyzer {

    private static final int FIRE = 1;
    private static final int PROTECT = 2;
    private static final int REGENERATE = 3;

    public static void analyze(JSONObject clientMessage1, JSONObject clientMessage2) {
        Integer idPlayer1 = clientMessage1.getInt("id");
        Integer idPlayer2 = clientMessage2.getInt("id");
        Integer actionPlayer1 = clientMessage1.getInt("action");
        Integer actionPlayer2 = clientMessage2.getInt("action");
        actionRealize(idPlayer1, actionPlayer1);
        actionRealize(idPlayer2, actionPlayer2);
    }

    private static void actionRealize(Integer id, Integer action) {
        switch (action) {
            case FIRE:
                Game.getGame().getRobot(id).setState(1);
                break;
            case PROTECT:
                Game.getGame().getRobot(id).setState(2);
                break;
            case REGENERATE:
                Game.getGame().getRobot(id).setState(3);
                break;
            default:
                System.out.println("Ошибочка с сообщением");
        }
    }
}
