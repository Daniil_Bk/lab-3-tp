import org.json.JSONObject;

import java.io.*;
import java.net.Socket;


public class ClientThread extends Thread {

    public static String ipAddr = "localhost";
    public static int port = 8080;

    public ClientThread(){
        start();
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(ipAddr, port);
            BufferedReader inputUser = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));


            System.out.println("Client started");

            while (true) {
                try {
                    JSONObject serverMessage = new JSONObject(in.readLine());
                    System.out.println(serverMessage.toString());
                    JSONObject message = new JSONObject();
                    message.put("id", serverMessage.getInt("id"));
                    Integer action = Integer.valueOf(inputUser.readLine());
                    while (action < 1 || action > 3) {
                        action = Integer.valueOf(inputUser.readLine());
                    }
                    message.put("action", action);
                    out.write(message.toString() + "\n");
                    out.flush();
                } catch (IOException e) {
                    try {
                        if (!socket.isClosed()) {
                            socket.close();
                            in.close();
                            out.close();
                        }
                    } catch (IOException ignored) {}
                }

            }
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
    }

}
