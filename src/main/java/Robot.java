public class Robot {
    private Integer hp;
    private Integer energy;
    private Integer state;

    public Robot(){
        this.hp = 5;
        this.energy = 5;
        this.state = 0; // 1-fire, 2-protect, 3-regenerate
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}

