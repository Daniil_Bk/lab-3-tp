
public class GameResponser {

    public static void response() {
        action(Game.getGame().getRobot(1).getState(), 1, 2);
        action(Game.getGame().getRobot(2).getState(), 2, 1);
    }

    private static void fire(Integer idPlayer, Integer idOpponent) {
        if (Game.getGame().getRobot(idOpponent).getState() != 2) {
            Integer hpOpponent = Game.getGame().getRobot(idOpponent).getHp();
            hpOpponent = hpOpponent - 1;
            if(idPlayer==2 && hpOpponent <= 0){
                Game.getGame().setState(false);
            }
                Game.getGame().getRobot(idOpponent).setHp(hpOpponent);
        }

        Integer energy = Game.getGame().getRobot(idPlayer).getEnergy();
        energy = energy - 1;
        Game.getGame().getRobot(idPlayer).setEnergy(energy);
    }

    private static void protect(Integer id) {
        Integer energy = Game.getGame().getRobot(id).getEnergy();
        energy = energy - 1;
        Game.getGame().getRobot(id).setEnergy(energy);
    }

    private static void regenerate(Integer id) {
        Game.getGame().getRobot(id).setEnergy(5);
    }

    private static void action(Integer state, Integer idPlayer, Integer idOpponent) {
        switch (state) {
            case 1:
                fire(idPlayer, idOpponent);
                break;
            case 2:
                protect(idPlayer);
                break;
            case 3:
                regenerate(idPlayer);
                break;
            default:
                System.out.println("Ошибочка с action");
        }
    }


}
